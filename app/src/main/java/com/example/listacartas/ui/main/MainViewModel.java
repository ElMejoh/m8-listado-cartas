package com.example.listacartas.ui.main;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.listacartas.ui.utilities.AppDatabase;
import com.example.listacartas.ui.utilities.Carta;
import com.example.listacartas.ui.utilities.CartaDao;
import com.example.listacartas.ui.utilities.CartasApi;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final CartaDao cartaDao;
    private LiveData<List<Carta>> cartas;


    public MainViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication()
        );
        this.cartaDao = appDatabase.getCartaDao();
    }

    public LiveData<List<Carta>> getCartas() {
        return cartaDao.getCartas();
    }

    public void reload() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        // Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(() -> {
            CartasApi api = new CartasApi();
            ArrayList<Carta> cartas = api.getCarta();

            cartaDao.deleteCartas();
            cartaDao.addCartas(cartas);

        });

    }


}