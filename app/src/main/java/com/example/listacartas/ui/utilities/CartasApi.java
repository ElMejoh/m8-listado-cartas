package com.example.listacartas.ui.utilities;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CartasApi {

    private final String BASE_URL = "https://api.magicthegathering.io";

    public ArrayList<Carta> getCarta(){
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .build();
        String url = builtUri.toString();
        return doCall(url);
    }

    // ORDENAR POR RAREZA
    ArrayList<Carta> getCartaByRarity(String rarity){
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .appendQueryParameter("rarity", rarity)
                .build();
        String url = builtUri.toString();
        return doCall(url);
    }

    // ORDENAR POR COLOR
    ArrayList<Carta> getCartaByColour(String colors){
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .appendQueryParameter("colors", colors)
                .build();
        String url = builtUri.toString();
        return doCall(url);
    }

    // ORDENAR POR COLOR
    ArrayList<Carta> getEveryFilter(String rarity, String colors){
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .appendQueryParameter("colors", colors)
                .appendQueryParameter("rarity", rarity)
                .build();
        String url = builtUri.toString();
        return doCall(url);
    }

    private ArrayList<Carta> doCall(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Carta> processJson(String jsonResponse) {
        ArrayList<Carta> cartasArrayFinal = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonCartasList = data.getJSONArray("cards");
            for (int i = 0; i < jsonCartasList.length(); i++) {
                JSONObject jsonCarta = jsonCartasList.getJSONObject(i);


                String auxImagenUrl;
                if (!jsonCarta.has("imageUrl")) auxImagenUrl = "https://www.incipiteditores.com/wp-content/themes/salient/img/perfil-hombre-vacio.jpg";
                else auxImagenUrl = jsonCarta.getString("imageUrl");

                int auxPower, auxToughness;
                if (!jsonCarta.has("power")) auxPower = -1;
                else auxPower = jsonCarta.getInt("power");
                if (!jsonCarta.has("toughness")) auxToughness = -1;
                else auxToughness = jsonCarta.getInt("toughness");

                Carta carta = new Carta(
                        jsonCarta.getString("name"),
                        jsonCarta.getString("manaCost"),
                        jsonCarta.getString("colors"),
                        jsonCarta.getString("rarity"),
                        jsonCarta.getString("type"),
                        jsonCarta.getString("text"),
                        auxImagenUrl,
                        auxPower,
                        auxToughness
                        );

                cartasArrayFinal.add(carta);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        return cartasArrayFinal;
    }

}
