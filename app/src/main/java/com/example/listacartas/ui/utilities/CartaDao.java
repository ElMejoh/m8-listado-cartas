package com.example.listacartas.ui.utilities;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CartaDao {

    @Query("SELECT * FROM CARTA")
    LiveData<List<Carta>> getCartas();

    @Insert
    void  addCarta(Carta carta);

    @Insert
    void  addCartas(List<Carta> cartas);

    @Delete
    void  deleteCarta(Carta carta);

    @Query("DELETE FROM CARTA")
    void deleteCartas();



}
