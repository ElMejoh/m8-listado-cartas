package com.example.listacartas.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.listacartas.DetailActivity;
import com.example.listacartas.R;
import com.example.listacartas.databinding.MainFragmentBinding;
import com.example.listacartas.ui.utilities.CardsAdapter;
import com.example.listacartas.ui.utilities.Carta;
import com.example.listacartas.ui.utilities.SettingsActivity;

import java.util.ArrayList;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    private ArrayAdapter<Carta> adapter;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        MainFragmentBinding binding = MainFragmentBinding.inflate(inflater);
        View view = binding.getRoot();
        // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String trait = preferences.getString("trait", "");





        // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



        adapter = new CardsAdapter(
                getContext(),
                R.layout.lv_cartas_row,
                new ArrayList<>()
        );
        binding.lvListadoCartas.setAdapter(adapter);



        // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



        binding.lvListadoCartas.setOnItemClickListener((parent, view1, position, id) -> {
            Carta carta = adapter.getItem(position);

            Log.d("carta", carta.toString());

            Intent i = new Intent(getContext(), DetailActivity.class);
            i.putExtra("carta", carta);

            startActivity(i);
        });



        // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        mViewModel.getCartas().observe(getViewLifecycleOwner(), cartas -> {
            adapter.clear();
            adapter.addAll(cartas);
        });


        // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


        refresh();
        return view;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cartas_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh){
            refresh();
        }

        if (id == R.id.action_settings){
            Intent i = new Intent(getContext(), SettingsActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        mViewModel.reload();

        // RefreshDataTask task = new RefreshDataTask();
        // task.execute();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }



    /*
    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Carta>> {
        @Override
        protected ArrayList<Carta> doInBackground(Void... voids) {
            CartasApi api = new CartasApi();

            return api.getCarta();
        }

        @Override
        protected void onPostExecute(ArrayList<Carta> cartas) {
            adapter.clear();
            for (Carta carta : cartas) {
                adapter.add(carta);
            }
        }
    }

     */


}