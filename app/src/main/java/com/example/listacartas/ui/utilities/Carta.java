package com.example.listacartas.ui.utilities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Carta implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String name;
    private String manaCost;
    private String colors;
    private String rarity;
    private String type;
    private String text;
    private String imageUrl;
    private int power;
    private int toughness;


    // CONTRUCTOR
    public Carta(String name, String manaCost, String colors, String rarity, String type, String text, String imageUrl, int power, int toughness) {
        this.name = name;
        this.manaCost = manaCost;
        this.colors = colors;
        this.rarity = rarity;
        this.type = type;
        this.text = text;
        this.imageUrl = imageUrl;
        this.power = power;
        this.toughness = toughness;
    }

    // CONTRUCTOR
    public Carta() {
        this.name = "";
        this.manaCost = "";
        this.colors = "";
        this.rarity = "";
        this.type = "";
        this.text = "";
        this.imageUrl = "";
        this.power = 0;
        this.toughness = 0;
    }

    // GETTERS
    public String getName() { return name; }
    public String getManaCost() { return manaCost; }
    public String getColors() { return colors; }
    public String getRarity() { return rarity; }
    public String getType() { return type; }
    public String getText() { return text; }
    public String getImageUrl() { return imageUrl; }
    public int getPower() { return power; }
    public int getToughness() { return toughness; }
    public int getId() { return id; }

    // SETTERS
    public void setName(String name) { this.name = name; }
    public void setManaCost(String manaCost) { this.manaCost = manaCost; }
    public void setColors(String colors){ this.colors = colors; }
    public void setRarity(String rarity) { this.rarity = rarity; }
    public void setType(String type) { this.type = type; }
    public void setText(String text) { this.text = text; }
    public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }
    public void setPower(int power) { this.power = power; }
    public void setToughness(int toughness) { this.toughness = toughness; }
    public void setId(int id) { this.id = id; }

    @Override
    public String toString() {
        return "\nCarta{" +
                "\nname='" + name + '\'' +
                "\nmanaCost='" + manaCost + '\'' +
                "\ncolors='" + colors + '\'' +
                "\nrarity='" + rarity + '\'' +
                "\ntype='" + type + '\'' +
                "\ntext='" + text + '\'' +
                "\nimageUrl='" + imageUrl + '\'' +
                "\npower='" + power + '\'' +
                "\ntoughness='" + toughness + '\'' +
                '}';
    }
}
