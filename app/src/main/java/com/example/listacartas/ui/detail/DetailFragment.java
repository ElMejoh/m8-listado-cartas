package com.example.listacartas.ui.detail;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.listacartas.R;
import com.example.listacartas.databinding.DetailFragmentBinding;
import com.example.listacartas.databinding.MainFragmentBinding;
import com.example.listacartas.ui.utilities.Carta;

public class DetailFragment extends Fragment {

    private DetailViewModel mViewModel;

    private DetailFragmentBinding binding;

    public static DetailFragment newInstance() {
        return new DetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DetailFragmentBinding.inflate(inflater);
        View view = binding.getRoot();


        Intent intent = getActivity().getIntent();
        if (intent != null){
            Log.d("intent", intent.toString());
            Carta carta = (Carta) intent.getSerializableExtra("carta");

            if (carta != null){
                showData(carta);
            }

        }

        return view;
    }

    private void showData(Carta carta) {
        binding.tvNameCardDetail.setText(carta.getName());
        binding.tvTypeCardDetail.setText(carta.getType());
        binding.tvRarityCardDetail.setText(carta.getRarity());
        binding.tvTextCardDetail.setText(carta.getText());
        binding.tvManaCostCardDetail.setText(carta.getManaCost());

        // SI EL PODER Y VIDA ES -1 (OSEA NO TIENE) NO MUESTRA EL PODER/VIDA
        if (carta.getToughness()==-1 && carta.getPower()==-1) binding.tvPowTouCardDetail.setText("");
        else binding.tvPowTouCardDetail.setText(carta.getPower()+"/"+carta.getToughness());

        // DEPENDIENDO DE LA RAREZA DE LA CARTA, CAMBIA EL COLOR DEL TEXTO
        switch (carta.getRarity()){
            case "Common":
                binding.tvRarityCardDetail.setTextColor(Color.parseColor("#070707"));
                break;
            case "Uncommon":
                binding.tvRarityCardDetail.setTextColor(Color.parseColor("#F3EBC0"));
                break;
            case "Rare":
                binding.tvRarityCardDetail.setTextColor(Color.parseColor("#F8FF09"));
                break;
            case "Mythical":
                binding.tvRarityCardDetail.setTextColor(Color.parseColor("#E91919"));
                break;
        }

        Glide.with(getContext())
                .load(carta.getImageUrl())
                .into(binding.imgVwCardImageDetail);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        // TODO: Use the ViewModel
    }

}