package com.example.listacartas.ui.utilities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.listacartas.R;

import java.util.List;

public class CardsAdapter extends ArrayAdapter<Carta> {
    public CardsAdapter(Context context, int resource, List<Carta> objects){
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Obtenim l'objecte en la possició corresponent
        Carta carta = getItem(position);

        // Mirem a veure si la View s'està reutilitzant, si no es així "inflem" la View
        // https://github.com/codepath/android_guides/wiki/Using-an-ArrayAdapter-with-ListView#row-view-recycling
        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_cartas_row, parent, false);
        }

        // Unim el codi en les Views del Layout
        TextView tv_nameCard = convertView.findViewById(R.id.tv_nameCard);
        TextView tv_colourCard = convertView.findViewById(R.id.tv_colourCard);
        TextView tv_typeCard = convertView.findViewById(R.id.tv_typeCard);
        ImageView iv_imagenCard = convertView.findViewById(R.id.iv_imagenCard);

        // Fiquem les dades dels objectes (provinents del JSON) en el layout
        tv_nameCard.setText(carta.getName());
        tv_colourCard.setText(carta.getColors());
        tv_typeCard.setText(carta.getType());

        // IMAGEN CON GLIDE
        Glide.with(getContext())
                .load(carta.getImageUrl())
                .into(iv_imagenCard);

        return convertView;
    }
}

